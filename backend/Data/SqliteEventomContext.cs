﻿namespace Eventom.Data;

using Microsoft.EntityFrameworkCore;

public class SqliteEventomContext : EventomContext
{
    private readonly Dbms dbOptions;

    public SqliteEventomContext(Dbms dbOptions) : base(dbOptions)
    {
        this.dbOptions = dbOptions;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite(dbOptions.ConnectionString);
    }
}
