using Eventom.Data;
using Eventom.TranslationService;
using Microsoft.EntityFrameworkCore;

namespace Eventom.EventManagement;

public class EventService
{
    private readonly IDbContextFactory<EventomContext> _contextFactory;
    private readonly ITranslationService _translationService;

    public EventService(IDbContextFactory<EventomContext> contextFactory, ITranslationService translationService)
    {
        _contextFactory = contextFactory;
        _translationService = translationService;
    }

    public async Task<Event> GetEventForLang(int id, string lang)
    {
        await using var db = await _contextFactory.CreateDbContextAsync();
        
        var dbEvent = await db.Event.Include(e => e.Descriptions).FirstAsync(e => e.Id == id);
        if (TargetDescLangIsPresent(dbEvent, lang))
        {
            return dbEvent.ToEvent(lang);
        }
        
        var translatedEvent = await TranslateAndStore(db, dbEvent, lang);
        return translatedEvent?.ToEvent(lang) 
               ?? throw new InvalidOperationException($"Translation failed for event with id: {id}");
    }

    public async IAsyncEnumerable<Event> GetEventsForLang(string lang)
    {
        await using var db = await _contextFactory.CreateDbContextAsync();

        var dbEvents = await db.Event
            .Include(e => e.Descriptions)
            .ToListAsync();
        
        foreach (var dbEvent in dbEvents)
        {
            if (TargetDescLangIsPresent(dbEvent, lang))
            {
                yield return dbEvent.ToEvent(lang);
            }
            else
            {
                var translatedEvent = await TranslateAndStore(db, dbEvent, lang);
                if (translatedEvent != null)
                {
                    yield return translatedEvent.ToEvent(lang);
                }
            }
        }
    }

    private static bool TargetDescLangIsPresent(DbEvent dbEvent, string lang)
    {
        return dbEvent.Descriptions.Any(description => description.Language == lang);
    }

    private async Task<DbEvent?> TranslateAndStore(DbContext db, DbEvent dbEvent, string lang)
    {
        var currentDesc = dbEvent.Descriptions.FirstOrDefault();
        if (currentDesc == null) return null;
        var translatedDesc = 
            await _translationService.TranslateTextAsync(currentDesc.Language, currentDesc.Description, lang);
            
        if (!string.IsNullOrEmpty(translatedDesc))
        {
            dbEvent.Descriptions.Add(new EventDescription
            {
                EventId = dbEvent.Id,
                Language = lang,
                Description = translatedDesc
            });
        }
        await db.SaveChangesAsync();
        return dbEvent;
    }
}