namespace Eventom.TranslationService.Google.Model; 

public class TranslationApiData {
    public List<TranslationApiTranslation> Translations { get; set; }
}