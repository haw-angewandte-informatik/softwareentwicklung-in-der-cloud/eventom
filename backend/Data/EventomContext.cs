﻿namespace Eventom.Data;
using Microsoft.EntityFrameworkCore;
using EventManagement;

public class EventomContext : DbContext
{
    private readonly Dbms dbOptions;

    public EventomContext(Dbms dbOptions)
    {
        this.dbOptions = dbOptions;
    }

    public DbSet<DbEvent> Event { get; set; } = default!;
    public DbSet<EventDescription> EventDescription { get; set; } = default!;
    public DbSet<DbUser> User { get; set; } = default!;
    public DbSet<Booking> Booking { get; set; } = default!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        //modelBuilder.Entity<EventDescription>(entity =>
        //{
        //    entity.HasKey(e => new { e.EventId, e.Language });
        //    entity
        //        .HasOne(e => e.Event)
        //        .WithMany(e => e.Descriptions)
        //        .HasForeignKey(e => e.EventId);
        //});

        modelBuilder.Entity<DbEvent>(entity =>
        {
            entity.HasKey(e => e.Id);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Name).IsRequired();
            entity.Property(e => e.OwnerEmail).IsRequired();
            entity.Property(e => e.Start).IsRequired();
            entity.Property(e => e.End).IsRequired();
            //entity
            //    .HasMany(e => e.Descriptions)
            //    .WithOne()
            //    .HasForeignKey(d => d.EventId)
            //    .HasPrincipalKey(e => e.Id)
            //    .OnDelete(DeleteBehavior.Cascade);
        });

        modelBuilder.Entity<DbUser>(entity =>
        {
            entity.HasKey(e => e.Email);
            entity.Property(e => e.Email).IsRequired();
            entity.Property(e => e.Organizer).IsRequired();
            entity.Property(e => e.PasswordHash).IsRequired();
            entity.Property(e => e.Salt).IsRequired();
        });

        modelBuilder.Entity<Booking>(entity =>
        {
            entity.HasKey(e => e.Id);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.HasOne<DbEvent>().WithMany().HasForeignKey(e => e.EventId).HasPrincipalKey(e => e.Id);
            entity.HasOne<DbUser>().WithMany().HasForeignKey(e => e.OwnerId).HasPrincipalKey(u => u.Email);
        });
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        switch (dbOptions.Type.ToLower())
        {
            case "sqlite":
                optionsBuilder.UseSqlite(dbOptions.ConnectionString);
                break;
            case "mysql":
                optionsBuilder.UseMySQL(dbOptions.ConnectionString);
                break;
            case "postgres":
            case "postgresql":
                optionsBuilder.UseNpgsql(dbOptions.ConnectionString);
                break;
        }
    }
}