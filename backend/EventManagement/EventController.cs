﻿// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Eventom.EventManagement;

using System.Security.Claims;
using Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;

[Route("api/[controller]")]
[ApiController]
public class EventController : ControllerBase
{
    private readonly IDbContextFactory<EventomContext> _contextFactory;
    private readonly EventService _eventService;
    private string ClientLanguage => Request.Headers["Accept-Language"].ToString().Split(",")[0]?.Split(';')[0]?.Trim()??"en";

    public EventController(IDbContextFactory<EventomContext> contextFactory, EventService eventService)
    {
        _contextFactory = contextFactory;
        _eventService = eventService;
    }

    // GET: api/<EventController>
    [HttpGet]
    public IAsyncEnumerable<Event> Get()
    {
        return _eventService.GetEventsForLang(ClientLanguage);
    }

    // GET api/<EventController>/5
    [HttpGet("{id:int}")]
    public async Task<Event> Get(int id)
    {
        return await _eventService.GetEventForLang(id, ClientLanguage);
    }

    // POST api/<EventController>
    [HttpPost]
    [Authorize(Roles = "Organizer")]
    public async Task<Event> CreateEvent([FromBody] NewEvent newEvent)
    {
        var userId = User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
        await using var db = await _contextFactory.CreateDbContextAsync();
        var createdEvent = new Event(0, userId, newEvent.Name, newEvent.Description ?? "", newEvent.Start, newEvent.End);
        db.Event.Add(DbEvent.FromEvent(createdEvent, ClientLanguage));
        await db.SaveChangesAsync();
        return createdEvent;
    }

    // PUT api/<EventController>/5
    [HttpPut("{id}")]
    [Authorize(Roles = "Organizer")]
    public async Task<Event> Put(int id, [FromBody] NewEvent eventUpdate)
    {
        var userId = User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
        await using var db = await _contextFactory.CreateDbContextAsync();
        var existingEvent = await db.Event.Include(e => e.Descriptions).FirstOrDefaultAsync(e => e.Id == id && e.OwnerEmail == userId);
        if (existingEvent == null)
        {
            throw new Exception("Event not found");
        }
        
        // Update event
        existingEvent.Name = eventUpdate.Name;
        existingEvent.Start = eventUpdate.Start;
        existingEvent.End = eventUpdate.End;

        // Update description, or remove if its empty and we still have one to fall back to
        var description = existingEvent.Descriptions.FirstOrDefault(d => d.Language == ClientLanguage);

        // None for this language
        if (description == null)
        {
            // New description is not empty
            if (!string.IsNullOrEmpty(eventUpdate.Description))
            {
                // Then save this description for this language
                existingEvent.Descriptions.Add(new EventDescription
                {
                    EventId = existingEvent.Id,
                    Language = ClientLanguage,
                    Description = eventUpdate.Description
                });
            }
            // New description is empty means we do nothing
        }
        // We have a description for this language
        else
        {
            // New description is not empty, update description
            if (!string.IsNullOrEmpty(eventUpdate.Description))
            {
                description.Description = eventUpdate.Description;
            }
            // New description is empty, remove description if we have another one to fall back to
            else if (existingEvent.Descriptions.Count > 1)
            {
                existingEvent.Descriptions.Remove(description);
            }
        }

        await db.SaveChangesAsync();
        return existingEvent.ToEvent(ClientLanguage);
    }

    // DELETE api/<EventController>/5
    [HttpDelete("{id}")]
    [Authorize(Roles = "Organizer")]
    public async Task Delete(int id)
    {
        var userId = User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
        await using var db = await _contextFactory.CreateDbContextAsync();
        var existingEvent = await db.Event.Include(e => e.Descriptions).FirstOrDefaultAsync(e => e.Id == id && e.OwnerEmail == userId);
        if (existingEvent == null)
        {
            throw new Exception("Event not found");
        }
        
        db.EventDescription.RemoveRange(existingEvent.Descriptions);
        db.Event.Remove(existingEvent);
        await db.SaveChangesAsync();
    }
}
