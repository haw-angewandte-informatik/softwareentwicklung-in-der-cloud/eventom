# Eventom

This application is a simple event management system. It is built with Svelte and dotnet.  
It uses JWT bearer authentication and EF Core for data access. Currently MySQL, SQLite, and Postgres are supported.

## Deployment

CI/CD automatically deploys the main branch to Google Cloud Run (backend) and Cloud Storage (frontend). The database is migrated automatically on startup.

This is the current deployment architecture:  
![Deployment architecture](./architecture.drawio.svg)

## Development

### Prerequisites

- [.NET 7 SDK](https://dotnet.microsoft.com/download/dotnet/7.0)
- [Node.js](https://nodejs.org/en/)

### Backend

The backend is a dotnet 7 web API. It uses EF Core for data access and JWT bearer authentication.  
It can be started with `dotnet run` in the `backend` directory. By default a new SQLite database called `Eventom` is created in the `backend` directory.  
There's a swagger UI available at `/swagger`.

Alternatively you can build the Dockerfile in the `backend` directory and run the container.

#### Configuration

The following environment variables can be used to configure the backend:

- Dbms__Type: The type of database to use. Currently supported: `mysql`, `sqlite`. Default: None, SQLite is used from the appsetting.development.json file.
- Dbms__ConnectionString: The connection string to the database. Default: None, SQLite is used from the appsetting.development.json file.

#### DB migrations

Migrations are automatically applied on startup. To create a new migration, run `dotnet ef migrations add <name> --context <db context> --provider <provider>` in the `backend` directory. This has to be run for each supported provider.

### Frontend

The frontend is a Svelte application. It can be started with `npm run dev` in the `frontend` directory.

#### Configuration

The httpService has a `baseUrl` property that can be used to configure the backend url. When the placeholder is not replaced, the frontend will default to the current Google Cloud Run deployment.
