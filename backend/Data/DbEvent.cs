﻿namespace Eventom.Data;

using System.ComponentModel.DataAnnotations;
using EventManagement;

public class DbEvent
{
    [Key]
    public int Id { get; set; }
    public string OwnerEmail { get; set; } = default!;
    public string Name { get; set; } = default!;
    public DateTime Start { get; set; }
    public DateTime End { get; set; }

    public virtual ICollection<EventDescription> Descriptions { get; set; } = default!;

    internal Event ToEvent(string languageCode)
    {
        return new(
            Id, 
            OwnerEmail, 
            Name, 
            (Descriptions.FirstOrDefault(d => d.Language == languageCode) ?? Descriptions.FirstOrDefault())?.Description ?? "", 
            Start, 
            End);
    }

    internal static DbEvent FromEvent(Event e, string languageCode)
    {
        return new DbEvent
        {
            Id = e.Id,
            OwnerEmail = e.OwnerEmail,
            Name = e.Name,
            Start = e.Start,
            End = e.End,
            Descriptions = new List<EventDescription>
            {
                new()
                {
                    EventId = e.Id,
                    Language = languageCode,
                    Description = e.Description
                }
            }
        };
    }
}
