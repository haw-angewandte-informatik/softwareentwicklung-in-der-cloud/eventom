namespace Eventom.TranslationService.Google.Model; 

public class TranslationApiTranslation {
    public string TranslatedText { get; set; }
    public string Model { get; set; }
}