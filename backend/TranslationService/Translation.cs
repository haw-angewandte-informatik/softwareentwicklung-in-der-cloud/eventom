namespace Eventom.TranslationService; 

public class Translation {
    public string ServiceProvider { get; set; }
    public string URI { get; set; }
    public string ApiKey { get; set; }
}