﻿namespace Eventom.Data;

using Microsoft.EntityFrameworkCore;

public class MySqlEventomContext : EventomContext
{
    private readonly Dbms dbOptions;

    public MySqlEventomContext(Dbms dbOptions) : base(dbOptions)
    {
        this.dbOptions = dbOptions;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseMySQL(dbOptions.ConnectionString);
    }
}
