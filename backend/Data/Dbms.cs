﻿namespace Eventom.Data;

public class Dbms
{
    public string Type { get; set; }
    public string ConnectionString { get; set; }
}
