namespace Eventom.TranslationService;

public interface ITranslationService {
    public Task<string> TranslateTextAsync(string sourceLanguageCode, string text, string targetLanguageCode);
}