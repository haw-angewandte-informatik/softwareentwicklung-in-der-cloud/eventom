﻿namespace Eventom.UserManagement;

using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

public class JwtService
{
    private readonly JwtSecurityTokenHandler tokenHandler = new();
    private readonly SigningCredentials credentials;
    private readonly string issuer;

    public JwtService(SecurityOptions secretOptions)
    {
        var secret = secretOptions.JwtSecretKey;
        var key = Encoding.UTF8.GetBytes(secret);
        var securityKey = new SymmetricSecurityKey(key);
        credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
        issuer = secretOptions.JwtIssuer;
    }

    public string CreateJwt(User user)
    {
        var claims = new List<Claim>
        {
            new(ClaimTypes.Role, user.Organizer ? "Organizer" : "Customer"),
            new(ClaimTypes.NameIdentifier, user.Email)
        };
        if (user.DisplayName is not null)
        {
            claims.Add(new(ClaimTypes.Name, user.DisplayName));
        }

        var token = new JwtSecurityToken(
            issuer: issuer,
            audience: null,
            claims: claims,
            expires: DateTime.UtcNow.AddYears(1),
            signingCredentials: credentials
        );
        return tokenHandler.WriteToken(token);
    }

    public ClaimsPrincipal? ValidateJwt(string token, string issuer)
    {
        var validationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = credentials.Key,
            ValidateIssuer = true,
            ValidIssuer = issuer,
            ValidateAudience = false,
            ValidateLifetime = true,
            ClockSkew = TimeSpan.Zero
        };

        try
        {
            var principal = tokenHandler.ValidateToken(token, validationParameters, out var validatedToken);
            return principal;
        }
        catch (Exception)
        {
            return null;
        }
    }

    public class SecurityOptions
    {
        public string JwtSecretKey { get; set; } = null!;
        public string JwtIssuer { get; set; } = null!;
    }
}

