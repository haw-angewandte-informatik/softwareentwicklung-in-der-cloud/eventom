namespace Eventom.TranslationService.Google.Model; 

public class TranslationRequest {
    public string SourceLanguageCode { get; set; }
    public string TargetLanguageCode { get; set; }
    public string Text { get; set; }
}