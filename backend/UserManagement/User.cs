﻿namespace Eventom.UserManagement;

public record User(string Email, bool Organizer, string? DisplayName = null);
public record Registration(string Email, string Password, bool Organizer, string? DisplayName = null);
public record Login(string Email, string Password);
