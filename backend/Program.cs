﻿using System.Configuration;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Eventom.Data;
using Eventom.EventManagement;
using Eventom.TranslationService;
using Eventom.UserManagement;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

// Gets the correct DB context based on the DBMS type
static EventomContext GetDbContext(Dbms options, IServiceScope scope) => options.Type.ToLower() switch
{
    "sqlite" => scope.ServiceProvider.GetRequiredService<SqliteEventomContext>(),
    "mysql" => scope.ServiceProvider.GetRequiredService<MySqlEventomContext>(),
    "postgres" or "postgresql" => scope.ServiceProvider.GetRequiredService<PostgresEventomContext>(),
    _ => throw new ConfigurationErrorsException("Invalid DBMS type")
};

var builder = WebApplication.CreateBuilder(args);

// Add Cloud Translation Service
var translationOptions = builder.Configuration.GetSection("Translation").Get<Translation>() ??
                         throw new ConfigurationErrorsException("Missing Translation options");
builder.Services.AddTranslationService(translationOptions);

// Get DB options from config (environment variables or appsettings.json)
var dbOptions = builder.Configuration.GetSection("Dbms").Get<Dbms>() ??
                throw new ConfigurationErrorsException("Missing DBMS options");

// Add EventService
builder.Services.AddScoped<EventService>();

// Add options and DB context to the container
builder.Services.AddSingleton(dbOptions);
builder.Services.AddDbContextFactory<EventomContext>();

// Only for migrations, don't use these contexts in the app as they work only for their respective DBMS
builder.Services.AddDbContext<MySqlEventomContext>();
builder.Services.AddDbContext<PostgresEventomContext>();
builder.Services.AddDbContext<SqliteEventomContext>();

// Configure user management
builder.Services.AddScoped<UserService>();
var securityOptions = builder.Configuration.GetSection("Security").Get<JwtService.SecurityOptions>() ??
                      throw new ConfigurationErrorsException("Missing security options");
builder.Services.AddSingleton(securityOptions);
builder.Services.AddSingleton<JwtService>();
builder.Services.AddAuthentication(opts =>
    {
        opts.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        opts.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    })
    .AddJwtBearer(opts => opts.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey =
            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityOptions.JwtSecretKey)),
        ValidateIssuer = false,
        ValidateAudience = false,
        ValidateLifetime = true,
        ClockSkew = TimeSpan.Zero
    });

// Configure web API
builder.Services.AddControllers();
builder.Services.AddCors(opts => opts.AddDefaultPolicy(policy =>
    policy.AllowAnyOrigin()
        .WithMethods("GET", "POST", "PUT", "DELETE").WithHeaders("Content-Type", "Authorization")
        .WithExposedHeaders("Authorization")));
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(opts =>
{
    opts.SwaggerDoc("v1", new OpenApiInfo { Title = "Eventom", Version = "v1" });
    opts.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Name = "Authorization",
        Description = "JWT Authorization header using the Bearer scheme.",
        Type = SecuritySchemeType.Http,
        Scheme = "Bearer",
        BearerFormat = "JWT",
        In = ParameterLocation.Header,
    });
    opts.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                }
            },
            Array.Empty<string>()
        }
    });
});

// Build the app with the above configuration
var app = builder.Build();

// Make sure the DB is initialized
using (var scope = app.Services.CreateScope())
{
    using var context = GetDbContext(dbOptions, scope);
    context.Database.Migrate();
}

// Allow CORS
app.UseCors();

// Swagger is always reachable
app.UseSwagger();
app.UseSwaggerUI();

// After swagger, it's all HTTPS and uses auth by default.
app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

// After auth, controllers are reachable
app.MapControllers();

app.Run();
