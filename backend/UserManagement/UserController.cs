﻿namespace Eventom.UserManagement;

using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

[Route("api/[controller]")]
[ApiController]
public class UserController : ControllerBase
{
    private readonly UserService userService;
    private readonly JwtService jwtService;

    public UserController(UserService userService, JwtService jwtService)
    {
        this.userService = userService;
        this.jwtService = jwtService;
    }

    // GET api/user/5
    [HttpPost("register")]
    public async Task<User> Register(Registration newUser)
    {
        var user = await userService.CreateNewUser(newUser);
        var jwt = jwtService.CreateJwt(user);
        Response.Headers.Authorization = "Bearer " + jwt;

        return user;
    }

    // POST api/user
    [HttpPost("login")]
    public async Task<User> Post([FromBody] Login login)
    {
        var user = await userService.Login(login);
        var jwt = jwtService.CreateJwt(user);
        Response.Headers.Authorization = "Bearer " + jwt;

        return user;
    }

    // DELETE api/user/5
    [HttpDelete]
    [Authorize]
    public async Task Delete(Login login)
    {
        var userId = User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
        if (userId != login.Email)
        {
            throw new Exception("You can only delete your own account");
        }

        Response.Cookies.Delete("jwt");
        await userService.Delete(login);
    }
}
