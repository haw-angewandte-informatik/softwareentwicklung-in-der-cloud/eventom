﻿namespace Eventom.UserManagement;

using Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

public class UserService
{
    private readonly IDbContextFactory<EventomContext> contextFactory;

    public UserService(IDbContextFactory<EventomContext> contextFactory)
    {
        this.contextFactory = contextFactory;
    }

    public async Task<User> CreateNewUser(Registration registration)
    {
        await using var db = await contextFactory.CreateDbContextAsync();
        if (await db.User.AnyAsync(u => u.Email == registration.Email))
            throw new Exception("User already exists");

        var (hash, salt) = PasswordHelper.HashPassword(registration.Password);
        var user = new DbUser
        {
            Email = registration.Email,
            Organizer = registration.Organizer,
            DisplayName = registration.DisplayName,
            PasswordHash = hash,
            Salt = salt
        };
        db.User.Add(user);
        await db.SaveChangesAsync();
        return new User(user.Email, user.Organizer, user.DisplayName);
    }

    public async Task<User> Login(Login login)
    {
        await using var db = await contextFactory.CreateDbContextAsync();
        var user = await db.User.AsNoTracking().FirstOrDefaultAsync(u => u.Email == login.Email);
        if (user == null)
            throw new Exception("User not found");

        if (!PasswordHelper.VerifyPassword(user.PasswordHash, user.Salt, login.Password))
            throw new Exception("Invalid password");

        return new User(user.Email, user.Organizer, user.DisplayName);
    }

    [Authorize]
    public async Task Delete(Login login)
    {
        await using var db = await contextFactory.CreateDbContextAsync();
        var user = await db.User.FirstOrDefaultAsync(u => u.Email == login.Email);
        if (user == null)
            throw new Exception("User not found");

        if (!PasswordHelper.VerifyPassword(user.PasswordHash, user.Salt, login.Password))
            throw new Exception("Invalid password");

        db.User.Remove(user);
        await db.SaveChangesAsync();
    }
}
