﻿namespace Eventom.Data;

using Microsoft.EntityFrameworkCore;

public class PostgresEventomContext : EventomContext
{
    private readonly Dbms dbOptions;

    public PostgresEventomContext(Dbms dbOptions): base(dbOptions)
    {
        this.dbOptions = dbOptions;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql(dbOptions.ConnectionString);
    }
}

