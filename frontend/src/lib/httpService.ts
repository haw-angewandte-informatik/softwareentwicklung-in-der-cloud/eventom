import { tokenStore, langStore } from './stores';

let token = null as string | null;
let lang = null as string | null;
let baseUri = "__BASE_URI__";

// Sync token,lang from store
tokenStore.subscribe(t => token = t);
langStore.subscribe(l => lang = l);
// In case the pipeline doesn't replace the baseUri, use the default one
if (!baseUri.startsWith("http")) {
    baseUri = "https://eventom-app-ah2ihcv2gq-ey.a.run.app";
}


// Construct headers with or without token
const headers = () => {
    return {
        'Content-Type': 'application/json',
        ...(token ? { 'Authorization': `Bearer ${token}` } : {}),
        ...(lang ? { 'Accept-Language': `${lang}` } : {})
    };
};

export const get = async <T>(path: string) => {
    const response = await fetch(`${baseUri}/${path}`, { headers: headers() });
    return await response.json() as T;
};

export const post = async <T>(path: string, data: any) => {
    const response = await fetch(`${baseUri}/${path}`, {
        method: 'POST',
        headers: headers(),
        body: JSON.stringify(data)
    });
    return await response.json() as T;
};

export const postRaw = async (path: string, data: any) => {
    const response = await fetch(`${baseUri}/${path}`, {
        method: 'POST',
        headers: headers(),
        body: data
    });
    return response;
}

export const put = async <T>(path: string, data: any) => {
    const response = await fetch(`${baseUri}/${path}`, {
        method: 'PUT',
        headers: headers(),
        body: JSON.stringify(data)
    });
    return await response.json() as T;
};

export const del = async (path: string) => {
    const response = await fetch(`${baseUri}/${path}`, {
        method: 'DELETE',
        headers: headers()
    });
    return response;
};
