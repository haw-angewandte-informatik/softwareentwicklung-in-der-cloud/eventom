﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Eventom.Migrations.MySql
{
    /// <inheritdoc />
    public partial class LocaliseEventDescription : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Event_User_OwnerEmail",
                table: "Event");

            migrationBuilder.DropIndex(
                name: "IX_Event_OwnerEmail",
                table: "Event");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Event");

            migrationBuilder.AlterColumn<string>(
                name: "OwnerEmail",
                table: "Event",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(256)");

            migrationBuilder.CreateTable(
                name: "EventDescription",
                columns: table => new
                {
                    EventId = table.Column<int>(type: "int", nullable: false),
                    Language = table.Column<string>(type: "varchar(255)", nullable: false),
                    Description = table.Column<string>(type: "longtext", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventDescription", x => new { x.EventId, x.Language });
                    table.ForeignKey(
                        name: "FK_EventDescription_Event_EventId",
                        column: x => x.EventId,
                        principalTable: "Event",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySQL:Charset", "utf8mb4");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EventDescription");

            migrationBuilder.AlterColumn<string>(
                name: "OwnerEmail",
                table: "Event",
                type: "varchar(256)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Event",
                type: "longtext",
                nullable: false);

            migrationBuilder.CreateIndex(
                name: "IX_Event_OwnerEmail",
                table: "Event",
                column: "OwnerEmail");

            migrationBuilder.AddForeignKey(
                name: "FK_Event_User_OwnerEmail",
                table: "Event",
                column: "OwnerEmail",
                principalTable: "User",
                principalColumn: "Email",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
