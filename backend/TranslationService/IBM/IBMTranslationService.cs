using System.Text;
using Newtonsoft.Json;

namespace Eventom.TranslationService.IBM; 

public class IBMTranslationService : ITranslationService {
    
    private readonly string _apiKey;
    private readonly string _apiUrl;

    public IBMTranslationService(string uri, string apiKey) {
        _apiUrl = uri;
        _apiKey = apiKey;
    }
    public async Task<string> TranslateTextAsync(string sourceLanguageCode, string text, string targetLanguageCode) {
        using var client = new HttpClient();
        client.DefaultRequestHeaders.Add("Authorization",
            $"Basic {Convert.ToBase64String(Encoding.UTF8.GetBytes($"apikey:{_apiKey}"))}");

        var url = $"{_apiUrl}/v3/translate?version=2018-05-01";
        
        var requestBody = new {
            text,
            source = sourceLanguageCode,
            target = targetLanguageCode
        };

        var response = await client.PostAsJsonAsync(url, requestBody);

        if (response.IsSuccessStatusCode) {
            var responseContent = await response.Content.ReadAsStringAsync();
            dynamic result = JsonConvert.DeserializeObject(responseContent) 
                             ?? throw new InvalidOperationException("No translation found in the response.");
            return result.translations[0].translation;
        }
        throw new HttpRequestException($"Error: {response.StatusCode} - {response.ReasonPhrase}");
    }
    
}
