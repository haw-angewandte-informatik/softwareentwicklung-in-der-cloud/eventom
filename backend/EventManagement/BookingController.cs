﻿namespace Eventom.EventManagement;

using System.Security.Claims;
using Eventom.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

[Route("api/[controller]")]
[ApiController]
public class BookingController : ControllerBase
{
    private readonly IDbContextFactory<EventomContext> contextFactory;

    public BookingController(IDbContextFactory<EventomContext> contextFactory)
    {
        this.contextFactory = contextFactory;
    }

    /// <summary>
    /// Returns all bookings for the logged in user.
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [Authorize]
    public async Task<IEnumerable<Booking>> Get()
    {
        var userId = User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
        await using var db = await contextFactory.CreateDbContextAsync();
        return await db.Booking.Where(b => b.OwnerId == userId).ToListAsync();
    }

    /// <summary>
    /// Returns a specific booking for the logged in user.
    /// </summary>
    /// <param name="id">ID of the booking you're looking for.</param>
    /// <returns>A Booking or null if not found.</returns>
    [HttpGet("{id}")]
    [Authorize]
    public async Task<Booking?> Get(int id)
    {
        var userId = User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
        await using var db = await contextFactory.CreateDbContextAsync();
        return await db.Booking.FirstOrDefaultAsync(b => b.Id == id && b.OwnerId == userId);
    }

    /// <summary>
    /// Creates a new booking for the logged in user.
    /// </summary>
    /// <param name="newBooking"></param>
    /// <returns>The newly created booking.</returns>
    [HttpPost]
    [Authorize]
    public Booking Post([FromBody] NewBooking newBooking)
    {
        var userId = User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
        var booking = new Booking(0, newBooking.EventId, userId, newBooking.Quantity, DateTime.Now);
        using var db = contextFactory.CreateDbContext();
        db.Booking.Add(booking);
        db.SaveChanges();
        return booking;
    }

    /// <summary>
    /// Deletes a booking for the logged in user. If the booking is not found, nothing happens.
    /// Trying to delete a booking of a different user will yield no result.
    /// </summary>
    /// <param name="id"></param>
    [HttpDelete("{id}")]
    [Authorize]
    public void Delete(int id)
    {
        var userId = User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
        using var db = contextFactory.CreateDbContext();
        var booking = db.Booking.FirstOrDefault(b => b.Id == id && b.OwnerId == userId);
        if (booking != null)
        {
            db.Booking.Remove(booking);
            db.SaveChanges();
        }
    }
}
