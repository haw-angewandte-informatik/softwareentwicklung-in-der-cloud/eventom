﻿namespace Eventom.UserManagement;

using System;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

public class PasswordHelper
{
    /// <summary>
    /// Creates a hash with new salt for the given password.
    /// </summary>
    /// <param name="password">Password provided by the user.</param>
    /// <returns>Finished password hash and the salt used on it to store.</returns>
    public static (string HashedPassword, byte[] Salt) HashPassword(string password)
    {
        // Generate a 128-bit salt
        var salt = new byte[128 / 8];
        using (var rng = RandomNumberGenerator.Create())
        {
            rng.GetBytes(salt);
        }

        // Derive a 256-bit hash
        var hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: password,
            salt: salt,
            prf: KeyDerivationPrf.HMACSHA256,
            iterationCount: 100000,
            numBytesRequested: 256 / 8));

        return (hashed, salt);
    }

    /// <summary>
    /// Verifies the given password against the stored hash.
    /// </summary>
    /// <param name="hashedPassword">Stored hash.</param>
    /// <param name="salt">Stored salt used to create the hash.</param>
    /// <param name="password">Password provided by the user.</param>
    /// <returns></returns>
    public static bool VerifyPassword(string hashedPassword, byte[] salt, string password)
    {
        // Derive a 256-bit hash
        var attemptedHash = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: password,
            salt: salt,
            prf: KeyDerivationPrf.HMACSHA256,
            iterationCount: 100000,
            numBytesRequested: 256 / 8));

        return hashedPassword == attemptedHash;
    }
}
