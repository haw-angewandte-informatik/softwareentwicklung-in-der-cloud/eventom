﻿namespace Eventom.EventManagement;

/// <summary>
/// An event
/// </summary>
/// <param name="Id">Unique identifier of this event</param>
/// <param name="OwnerEmail">Owner/creator of this event</param>
/// <param name="Name">Name of this event</param>
/// <param name="Description">Description of this event</param>
/// <param name="Start">When this event starts</param>
/// <param name="End">When this event ends</param>
public record Event(int Id, string OwnerEmail, string Name, string Description, DateTime Start, DateTime End);
public record NewEvent(string Name, string? Description, DateTime Start, DateTime End);