﻿namespace Eventom.EventManagement;

public record Booking(int Id, int EventId, string OwnerId, int Quantity, DateTime CreatedAt);
public record NewBooking(int EventId, int Quantity);