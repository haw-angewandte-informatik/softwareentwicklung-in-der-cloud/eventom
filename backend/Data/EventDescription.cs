﻿namespace Eventom.Data;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

[PrimaryKey(nameof(EventId), nameof(Language))]
public class EventDescription
{
    [Key]
    [ForeignKey("Event")]
    public int EventId { get; set; }
    [Key]
    [MinLength(2)]
    public string Language { get; set; } = default!;
    [MinLength(1)]
    public string Description { get; set; } = default!;

    public virtual DbEvent Event { get; set; } = default!;
}
