import { writable } from 'svelte/store';
import type { User } from './types';

export const userStore = writable<User | null>(null);
export const tokenStore = writable<string | null>(null);
export const langStore = writable<string>('en');


// Make sure this isn't called during prerendering
if (typeof window !== 'undefined') {
    // On init, check if there is a user in local storage
    // If so, set it as the userStore
    const user = localStorage.getItem('user');
    if (user) {
        userStore.set(JSON.parse(user));
    }

    // Subscribe to userStore and save it in local storage
    userStore.subscribe(user => {
        if (user) {
            localStorage.setItem('user', JSON.stringify(user));
        } else {
            localStorage.removeItem('user');
        }
    });

    // Same for the token
    const token = localStorage.getItem('token');
    if (token) {
        tokenStore.set(token);
    }

    tokenStore.subscribe(token => {
        if (token) {
            localStorage.setItem('token', token);
        } else {
            localStorage.removeItem('token');
        }
    });

    // Same for the language
    let lang = localStorage.getItem('language') || navigator.language.split('-')[0];
    if (lang) {
        langStore.set(lang);
    }

    langStore.subscribe(lang => {
        if (lang) {
            localStorage.setItem('language', lang);
        } else {
            localStorage.removeItem('language');
        }
    });

}
