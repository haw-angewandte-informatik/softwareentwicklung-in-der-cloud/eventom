using Eventom.TranslationService.Google.Model;
using Newtonsoft.Json;

namespace Eventom.TranslationService.Google;

public class GoogleTranslationService : ITranslationService {

    private readonly string _apiKey;
    private readonly string _apiUrl;

    public GoogleTranslationService(string uri, string apiKey) {
        _apiUrl = uri;
        _apiKey = apiKey;
    }

    public async Task<string> TranslateTextAsync(string sourceLanguageCode, string text, string targetLanguageCode) {

        var payload = new List<KeyValuePair<string, string>> {
            new("q", text),
            new("source", sourceLanguageCode),
            new("target", targetLanguageCode),
            new("format", "text"),
            new("model", "nmt"),
            new("key", _apiKey)
        };

        using var client = new HttpClient();

        var requestUrl =
            $"{_apiUrl}?{string.Join("&", payload.Select(p => $"{p.Key}={Uri.EscapeDataString(p.Value)}"))}";

        client.BaseAddress = new Uri(requestUrl);

        var content = new FormUrlEncodedContent(payload);

        var response = await client.PostAsync("", content);

        if (response.IsSuccessStatusCode) {
           
            var responseBody = await response.Content.ReadAsStringAsync();
            var translationResponse = JsonConvert.DeserializeObject<TranslationApiResponse>(responseBody);
            
            var translation = translationResponse?.Data?.Translations?.FirstOrDefault();
            
            if (translation != null) {
                return translation.TranslatedText;
            }
          
            throw new InvalidOperationException("No translation found in the response.");
           
        }
        
        throw new HttpRequestException($"Error: {response.StatusCode} - {response.ReasonPhrase}");
    }
}
