﻿namespace Eventom.Data;

using System.ComponentModel.DataAnnotations.Schema;

public class DbUser
{
    [Column(TypeName = "varchar(256)")]
    public string Email { get; set; } = default!;
    public bool Organizer { get; set; }
    public string? DisplayName { get; set; }
    public string PasswordHash { get; set; } = default!;
    public byte[] Salt { get; set; } = default!;
}
