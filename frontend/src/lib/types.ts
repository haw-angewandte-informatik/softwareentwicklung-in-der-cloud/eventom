export interface User {
    email: string;
    organizer: boolean;
    displayName: string;
}

export interface Event {
    id: number;
    ownerEmail: string;
    name: string;
    description: string;
    start: string; // ISO 8601 format date-time string
    end: string; // ISO 8601 format date-time string
}

export interface Booking {
    id: number;
    eventId: number;
    ownerId: string;
    quantity: number;
    createdAt: string; // ISO 8601 format date-time string
}
