using Eventom.TranslationService.Google;
using Eventom.TranslationService.IBM;

namespace Eventom.TranslationService;

public static class TranslationServiceInjection {
    
    public static void AddTranslationService(this IServiceCollection services, Translation translationOptions) {
        switch (translationOptions.ServiceProvider) {
            case "GCP":
                services.AddScoped<ITranslationService>(
                    _ => new GoogleTranslationService(translationOptions.URI, translationOptions.ApiKey));
                break;
            case "IBM":
                services.AddScoped<ITranslationService>(
                    _ => new IBMTranslationService(translationOptions.URI, translationOptions.ApiKey));
                break;
            default:
                throw new ArgumentException($"{translationOptions.ServiceProvider} is not a valid provider.");
        }
    }
    
}
